import UIKit

class EventCell: UITableViewCell {

    @IBOutlet private weak var eventDateLabel: UILabel!
    @IBOutlet private weak var teamOneLabel: UILabel!
    @IBOutlet private weak var teamTwoLabel: UILabel!

    func setup(event: EventResponse) {
        if let homeTeam =  event.strHomeTeam {
            self.teamOneLabel.text = homeTeam
        }
        if let awayTeam = event.strAwayTeam {
            self.teamTwoLabel.text = awayTeam
        }
        if let date =  event.dateEvent {
            self.eventDateLabel.text = date
        }
    }

}
