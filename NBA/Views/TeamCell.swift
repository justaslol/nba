import UIKit

class TeamCell: UITableViewCell {

    @IBOutlet private weak var teamNameLabel: UILabel!
    @IBOutlet private weak var teamIcon: UIImageView!
    @IBOutlet private weak var teamDescriptionLabel: UILabel!
    @IBOutlet private weak var borderView: UIView!

    func addBorder( cell: TeamCell) {
        cell.borderView.layer.cornerRadius = 10
        cell.borderView.layer.borderWidth = 1.0
        cell.borderView.layer.borderColor = UIColor.black.cgColor
        cell.borderView.layer.shadowColor = UIColor.black.cgColor
        cell.borderView.layer.shadowOffset = CGSize(width: 0, height: 3)
        cell.borderView.layer.shadowOpacity = 0.7
        cell.borderView.layer.shadowRadius = 4.0
    }

    func setup(team: TeamResponse) {
        if let text = team.strTeam {
            self.teamNameLabel.text = text
        }
        if let description = team.strDescriptionEN {
            self.teamDescriptionLabel.text = description
        }
        if let imageUrl = team.strTeamBadge {
            self.teamIcon.downloaded(from: imageUrl)
        }
    }
}
