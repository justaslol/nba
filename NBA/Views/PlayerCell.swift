import UIKit

class PlayerCell: UITableViewCell {

    @IBOutlet private weak var playerImage: UIImageView!
    @IBOutlet private weak var playerNameLabel: UILabel!

    func setup(player: PlayerResponse) {

        if let playerName = player.strPlayer {
            self.playerNameLabel.text = playerName
        }
        if let url = player.strThumb {
            self.playerImage.downloaded(from: url)
        }
    }
}
