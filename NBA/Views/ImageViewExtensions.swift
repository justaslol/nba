import UIKit

extension UIImageView {

    func downloaded(from url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFill) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async {
                self.image = image

            }
        }.resume()
    }
    func downloaded(from link: String, contentMode mode: UIView.ContentMode = .scaleAspectFill) {
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
    func focusOnFace(imageView:inout UIImageView ) {

        guard let image = imageView.image,
              var personImage = CIImage(image: image) else { return }

        let accuracy = [CIDetectorAccuracy: CIDetectorAccuracyLow]
        let faceDetector = CIDetector(ofType: CIDetectorTypeFace, context: nil, options: accuracy)
        guard let face = faceDetector?.features(in: personImage).first as? CIFaceFeature else { return }

        var rect = face.bounds
        rect.size.height = max(face.bounds.height, face.bounds.width)
        rect.size.width = max(face.bounds.height, face.bounds.width)
        rect = rect.insetBy(dx: -150, dy: -150) // Adds padding around the face so it's not so tightly cropped

        personImage = personImage.cropped(to: rect)

        imageView.image = UIImage(ciImage: personImage)

    }
}
