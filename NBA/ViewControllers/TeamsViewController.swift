import UIKit

class TeamsViewController: UIViewController {
    
    var teams: [TeamResponse] = []
    let updateHandler: UpdateHandling = UpdateHandler(data: RealmHandler(), apiHandler: APIHandler())
    let dispatchQueue = DispatchQueue.global(qos: .background)
    let refreshControl = UIRefreshControl()
    
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidAppear(_ animated: Bool) {
        
        dispatchQueue.async {
            self.updateHandler.getTeamData { [weak self] (teams, error) in
                guard let self = self else { return }
                if let error = error {
                    print(error)
                }
                self.teams = teams.sorted { ($0.strTeam ?? "") < ($1.strTeam ?? "")}
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigationBarItems()
        
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tableView.refreshControl = refreshControl
    }
    
    @objc func refresh() {
        dispatchQueue.async {
            self.updateHandler.getTeamData { [weak self] (teams, error) in
                guard let self = self else { return }
                if let error = error {
                    print(error)
                }
                self.teams = teams.sorted { ($0.strTeam ?? "") < ($1.strTeam ?? "")}
                DispatchQueue.main.async {
                    self.refreshControl.endRefreshing()
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    func setupNavigationBarItems() {
        let titleImageView = UIImageView(image: #imageLiteral(resourceName: "nav_logoman"))
        titleImageView.widthAnchor.constraint(equalToConstant: 64).isActive = true
        titleImageView.heightAnchor.constraint(equalToConstant: 44).isActive = true
        titleImageView.contentMode = .scaleAspectFit
        navigationItem.titleView = titleImageView
        
        let listButton = UIButton(type: .system)
        listButton.setImage(#imageLiteral(resourceName: "list_view").withTintColor(UIColor(#colorLiteral(red: 0.1047369084, green: 0.2277311169, blue: 0.4619410516, alpha: 1)), renderingMode: .alwaysTemplate), for: .normal)
        listButton.widthAnchor.constraint(equalToConstant: 44).isActive = true
        listButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        let gridButton = UIButton(type: .system)
        gridButton.setImage(#imageLiteral(resourceName: "grid_view").withRenderingMode(.alwaysTemplate), for: .normal)
        gridButton.widthAnchor.constraint(equalToConstant: 44).isActive = true
        gridButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        navigationItem.rightBarButtonItems =  [UIBarButtonItem(customView: gridButton), UIBarButtonItem(customView: listButton)]
        
    }
}
extension TeamsViewController:  UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return teams.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "teamCell", for: indexPath) as! TeamCell
        cell.setup(team: teams[indexPath.row])
        cell.addBorder(cell: cell)
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let teamDetailsController = segue.destination as? TeamDetailsViewController else {
            fatalError("Unexpected destination: \(segue.destination)")
        }
        guard let selectedTeamCell = sender as? TeamCell else {
            fatalError("Unexpected sender: \(String(describing: sender))")
        }
        guard let indexPath = tableView.indexPath(for: selectedTeamCell) else {
            fatalError("The selected cell is not being displayed by the table")
        }
        teamDetailsController.team = teams[indexPath.row]
    }
}

extension TeamsViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return teams.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "teamCell", for: indexPath) as! TeamCollectionViewCell
        if let imageUrl = teams[indexPath.row].strTeamBadge {
            cell.teamImage.downloaded(from: imageUrl)
        }
        return cell
    }
}
