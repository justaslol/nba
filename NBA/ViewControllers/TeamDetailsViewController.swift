import UIKit

enum Segues: String {
    case teamMatches = "teamMatches"
    case teamPlayers = "teamPlayers"
}

class TeamDetailsViewController: UIViewController {

    var teamName: String?
    var teamStadiumUrl: String?
    var team: TeamResponse?
    let alert = UIAlertController(title: "Page Not Found",
                                  message: "The information you are trying to reach is not available.",
                                  preferredStyle: .alert)

    @IBOutlet private weak var teamDetailsNewsButton: UIView!
    @IBOutlet private weak var teamDetailsPlayersButton: UIView!
    @IBOutlet private weak var teamNameLabel: UILabel!
    @IBOutlet private weak var teamImage: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        if let text = team?.strTeam {
            teamNameLabel.attributedText = NSMutableAttributedString(string: text, attributes: strokeTextAttributes)
        }
        teamImage.downloaded(from: team?.strStadiumThumb ?? "")
    }

    @IBAction func switchViews(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            teamDetailsNewsButton.alpha = 1
            teamDetailsPlayersButton.alpha = 0
        } else {
            teamDetailsNewsButton.alpha = 0
            teamDetailsPlayersButton.alpha = 1
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        switch(segue.identifier ?? "") {
        case Segues.teamMatches.rawValue:
            guard let teamEventsController = segue.destination as? TeamEventsViewController else {
                return
            }
            teamEventsController.teamId = self.team?.idTeam ?? ""

        case Segues.teamPlayers.rawValue:
            guard let teamPlayersController = segue.destination as? TeamPlayersViewController else {
                return
            }
            if let teamId = self.team?.idTeam {
                teamPlayersController.teamId = teamId
            }
            if let name = self.team?.strTeam {
                teamPlayersController.teamName = name
            }
        default:
            alert.addAction(UIAlertAction(title: "Discard", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
        }

    let strokeTextAttributes = [
    NSAttributedString.Key.strokeColor : UIColor.black,
    NSAttributedString.Key.foregroundColor : UIColor.white,
    NSAttributedString.Key.strokeWidth : -4.0
        ]
    as [NSAttributedString.Key : Any]
}
