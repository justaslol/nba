import UIKit

class PlayerViewController: UIViewController {

    var player: PlayerResponse?

    @IBOutlet private weak var playerImage: UIImageView!
    @IBOutlet private weak var playerAgeLabel: UILabel!
    @IBOutlet private weak var playerHeightLabel: UILabel!
    @IBOutlet private weak var playerWeightLabel: UILabel!
    @IBOutlet private weak var playerDescriptionLabel: UILabel!
    @IBOutlet private weak var playerNameLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        if let image = player!.strThumb {
            playerImage.downloaded(from: image)

        }
        if let date = player!.dateBorn {
            let formatedDate = getAgeFromDOF(date: date)
            playerAgeLabel.text = "Age\n\(formatedDate.0)"
        }
        if let height = player!.strHeight {
            playerHeightLabel.text = "Height\n\(height)"
        }
        if let weight = player!.strWeight {
            playerWeightLabel.text = "Weight\n\(weight)"
        }
        if let description = player!.strDescriptionEN {
            playerDescriptionLabel.text = description
        }
        if let name = player!.strPlayer {
            playerNameLabel.attributedText = NSMutableAttributedString(string: name, attributes: strokeTextAttributes)
        }

    }
    
    let strokeTextAttributes = [
    NSAttributedString.Key.strokeColor : UIColor.black,
    NSAttributedString.Key.foregroundColor : UIColor.white,
    NSAttributedString.Key.strokeWidth : -4.0
        ]
    as [NSAttributedString.Key : Any]

    func getAgeFromDOF(date: String) -> (Int,Int,Int) {

        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "YYYY-MM-dd"
        let dateOfBirth = dateFormater.date(from: date)

        let calender = Calendar.current

        let dateComponent = calender.dateComponents([.year, .month, .day], from:
        dateOfBirth!, to: Date())

        return (dateComponent.year!, dateComponent.month!, dateComponent.day!)
    }

}
