import UIKit

class TeamPlayersViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var players: [PlayerResponse] = []
    let updateHandler: UpdateHandling = UpdateHandler(data: RealmHandler(), apiHandler: APIHandler())
    var teamId = String()
    var teamName = String()
    let refreshControl = UIRefreshControl()
    let dispatchQueue = DispatchQueue.global(qos: .background)
    let alert = UIAlertController(title: "Error occured", message: "An error occured while trying to reach player details", preferredStyle: .alert)
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        
        tableView.rowHeight = 69.0
        
        dispatchQueue.async {
            self.updateHandler.getPlayerData(teamId: self.teamId) { [weak self] (players) in
                guard let self = self else { return }
                self.players = players.sorted { ($0.strPlayer ?? "") < ($1.strPlayer ?? "") }
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
        
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tableView.refreshControl = refreshControl
    }
    
    @objc func refresh() {
        dispatchQueue.async {
            self.updateHandler.getPlayerData(teamId: self.teamId) { [weak self] (players) in
                guard let self = self else { return }
                self.players = players.sorted { ($0.strPlayer ?? "") < ($1.strPlayer ?? "") }
                DispatchQueue.main.async {
                    self.refreshControl.endRefreshing()
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return players.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "playerCell", for: indexPath) as! PlayerCell
        cell.setup(player: players[indexPath.row])
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let playerController = segue.destination as? PlayerViewController else {
            alert.addAction(UIAlertAction(title: "Discard", style: .default, handler: nil))
            self.present(alert, animated: true)
            return
        }
        guard let selectedPlayerCell = sender as? PlayerCell else {
            alert.addAction(UIAlertAction(title: "Discard", style: .default, handler: nil))
            self.present(alert, animated: true)
            return
        }
        guard let indexPath = tableView.indexPath(for: selectedPlayerCell) else {
            alert.addAction(UIAlertAction(title: "Discard", style: .default, handler: nil))
            self.present(alert, animated: true)
            return
        }
        let selectedPlayer = players[indexPath.row]
        playerController.player = selectedPlayer
    }
}
