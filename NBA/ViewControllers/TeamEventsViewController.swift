import UIKit

class TeamEventsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var events: [EventResponse] = []
    var teamId = String()
    let dispatchQueue = DispatchQueue.global(qos: .background)
    let updateHandler: UpdateHandling = UpdateHandler(data: RealmHandler(), apiHandler: APIHandler())
    let refreshControl = UIRefreshControl()

    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        dispatchQueue.async {
            self.updateHandler.getEventData(teamId: self.teamId) { [weak self] (events) in
                guard let self = self else { return }
                self.events = events.sorted { ($0.dateEvent ?? "") < ($1.dateEvent ?? "") }
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tableView.refreshControl = refreshControl
    }

    @objc func refresh() {
        dispatchQueue.async {
            self.updateHandler.getEventData(teamId: self.teamId) { [weak self] (events) in
                guard let self = self else { return }
                self.events = events.sorted { ($0.dateEvent ?? "") < ($1.dateEvent ?? "") }
                DispatchQueue.main.async {
                    self.refreshControl.endRefreshing()
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return events.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "matchCell", for: indexPath) as! EventCell

        cell.setup(event: events[indexPath.row])

        return cell
    }

}
