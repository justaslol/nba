import Foundation
import RealmSwift

protocol EventPersistable {
    associatedtype ManagedObject: RealmSwift.Object
    init(managedObject: ManagedObject)
    func managedObject(teamId: String) -> RealmEvent
}

struct EventResponse {

    let strHomeTeam: String?
    let strAwayTeam: String?
    let dateEvent: String?
}

extension EventResponse: Decodable, EventPersistable {
    enum CodingKeys: String, CodingKey {
        case strHomeTeam = "strHomeTeam"
        case strAwayTeam = "strAwayTeam"
        case dateEvent = "dateEvent"
    }
    
    public init(managedObject: RealmEvent) {
        strHomeTeam = managedObject.strHomeTeam
        strAwayTeam = managedObject.strAwayTeam
        dateEvent = managedObject.dateEvent
    }
    public func managedObject(teamId: String) -> RealmEvent {
        let event = RealmEvent()
        event.strHomeTeam = strHomeTeam
        event.strAwayTeam = strAwayTeam
        event.dateEvent = dateEvent
        event.teamId = teamId
        event.compoundKey = randomString(length: 8)
        return event
    }
    
    func randomString(length: Int) -> String {
      let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
      return String((0..<length).map{ _ in letters.randomElement()! })
    }
}

struct EventsResponse {

    let events: [EventResponse]?
}

extension EventsResponse: Decodable {
    enum CodingKeys: String, CodingKey {
        case events = "results"
    }
}

class RealmEvent: Object {
    @objc dynamic var strHomeTeam: String? = nil
    @objc dynamic var strAwayTeam: String? = nil
    @objc dynamic var dateEvent: String? = nil
    @objc dynamic var teamId: String? = nil
    @objc dynamic var compoundKey: String? = nil
    
    override static func primaryKey() -> String? {
        return "compoundKey"
    }
}
