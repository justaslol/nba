import Foundation

enum TeamEntity: String {
    case entityName = "Team"
    case teamId = "idTeam"
    case name = "strTeam"
    case description = "strDescriptionEN"
    case imageUrl = "strTeamBadge"
    case stadiumImageUrl = "strStadiumThumb"
}

enum EventEntity: String {
    case entityName = "Event"
    case homeTeam = "homeTeam"
    case date = "date"
    case awayTeam = "awayTeam"
}

enum PlayerEntity: String {
    case entityName = "Player"
    case teamId = "idTeam"
    case playerId = "idPlayer"
    case name = "strPlayer"
    case description = "strDescriptionEN"
    case born = "dateBorn"
    case height = "strHeight"
    case weight = "strWeight"
    case imageUrl = "strThumb"
    case teamName = "strTeam"
}
