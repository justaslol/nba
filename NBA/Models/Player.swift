import Foundation
import RealmSwift

struct PlayerResponse {

    let idPlayer: String?
    let idTeam: String?
    let strTeam: String?
    let strPlayer: String?
    let dateBorn: String?
    let strHeight: String?
    let strWeight: String?
    let strThumb: String?
    let strDescriptionEN: String?
}

extension PlayerResponse: Decodable, Persistable {
    enum CodingKeys: String, CodingKey {
        case idPlayer = "idPlayer"
        case idTeam = "idTeam"
        case strTeam = "strTeam"
        case strPlayer = "strPlayer"
        case dateBorn = "dateBorn"
        case strHeight = "strHeight"
        case strWeight = "strWeight"
        case strThumb = "strThumb"
        case strDescriptionEN = "strDescriptionEN"
    }
    
    public init(managedObject: RealmPlayer) {
        idPlayer = managedObject.idPlayer
        idTeam = managedObject.idTeam
        strTeam = managedObject.strTeam
        strPlayer = managedObject.strPlayer
        dateBorn = managedObject.dateBorn
        strHeight = managedObject.strHeight
        strWeight = managedObject.strWeight
        strThumb = managedObject.strThumb
        strDescriptionEN = managedObject.strDescriptionEN
    }
    
    public func managedObject() -> RealmPlayer {
        let player = RealmPlayer()
        player.idPlayer = idPlayer
        player.idTeam = idTeam
        player.strTeam = strTeam
        player.strPlayer = strPlayer
        player.dateBorn = dateBorn
        player.strHeight = strHeight
        player.strWeight = strWeight
        player.strThumb = strThumb
        player.strDescriptionEN = strDescriptionEN
        
        return player
    }
}

struct PlayersResponse {

    let players: [PlayerResponse]?
}

extension PlayersResponse: Decodable {
    enum CodingKeys: String, CodingKey {
        case players = "player"
    }
}

class RealmPlayer: Object {
    @objc dynamic var idPlayer: String? = nil
    @objc dynamic var idTeam: String? = nil
    @objc dynamic var strTeam: String? = nil
    @objc dynamic var strPlayer: String? = nil
    @objc dynamic var dateBorn: String? = nil
    @objc dynamic var strHeight: String? = nil
    @objc dynamic var strWeight: String? = nil
    @objc dynamic var strThumb: String? = nil
    @objc dynamic var strDescriptionEN: String? = nil
    
    override static func primaryKey() -> String? {
        return "idPlayer"
    }
}
