import Foundation
import RealmSwift

public protocol Persistable {
    associatedtype ManagedObject: RealmSwift.Object
    init(managedObject: ManagedObject)
    func managedObject() -> ManagedObject
}

struct TeamResponse {
    let idTeam: String?
    let strTeam: String?
    let strDescriptionEN: String?
    let strTeamBadge: String?
    let strStadiumThumb: String?
    
}

extension TeamResponse: Codable, Persistable {
    enum CodingKeys: String, CodingKey {
        case idTeam = "idTeam"
        case strTeam = "strTeam"
        case strDescriptionEN = "strDescriptionEN"
        case strTeamBadge = "strTeamBadge"
        case strStadiumThumb = "strStadiumThumb"
    }
    
    public init(managedObject: RealmTeam) {
        idTeam = managedObject.idTeam
        strTeam = managedObject.strTeam
        strDescriptionEN = managedObject.strDescriptionEN
        strTeamBadge = managedObject.strTeamBadge
        strStadiumThumb = managedObject.strStadiumThumb
    }
    public func managedObject() -> RealmTeam {
        let team = RealmTeam()
        team.idTeam = idTeam
        team.strTeam = strTeam
        team.strDescriptionEN = strDescriptionEN
        team.strTeamBadge = strTeamBadge
        team.strStadiumThumb = strStadiumThumb
        return team
    }
}

struct TeamsResponse: Codable {
    
    let teams: [TeamResponse]?
}

class RealmTeam: Object {
    @objc dynamic var idTeam: String? = nil
    @objc dynamic var strTeam: String? = nil
    @objc dynamic var strDescriptionEN: String? = nil
    @objc dynamic var strTeamBadge: String? = nil
    @objc dynamic var strStadiumThumb: String? = nil
    
    override static func primaryKey() -> String? {
        return "idTeam"
    }
}
