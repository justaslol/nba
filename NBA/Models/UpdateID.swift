import Foundation

enum UpdateID: String {
    case team = "1"
    case event = "2"
    case player = "3"
}

enum UpdateInterval: Int {
    case team = 3600
    case event = 900
    case player = 1800
}
