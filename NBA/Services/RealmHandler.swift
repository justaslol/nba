import Foundation
import RealmSwift

protocol DataHandling {
    func fetchTeams(completionHandler: @escaping ([TeamResponse], Error?) -> Void)
    func createTeamData(teams: [TeamResponse])
    func getTeamList(teamHandler: @escaping ([String]) -> Void)
    func getTeamIds(teamHandler: @escaping ([String]) -> Void)
    func createEventData(events: [EventResponse], teamId: String)
    func fetchEventsById(teamId: String, completionHandler: @escaping ([EventResponse]) -> Void)
    func createPlayerData(players: [PlayerResponse])
    func fetchPlayers(teamId: String, completionHandler: @escaping ([PlayerResponse]) -> Void)
    func deleteData(entityName: String)
    func isEmpty(entityName: String) -> Bool
    
}

class RealmHandler {}

extension RealmHandler: DataHandling {
    func fetchTeams(completionHandler: @escaping ([TeamResponse], Error?) -> Void) {
        guard let realm = try? Realm() else { return }
        debugPrint("fetch teams realm")
        print(Realm.Configuration.defaultConfiguration.fileURL!)
        var teams: [TeamResponse] = []
        let teamsObj = realm.objects(RealmTeam.self)
        
        for teamObj in teamsObj {
            teams.append(TeamResponse(managedObject: teamObj))
        }
        
        completionHandler(teams, nil)
    }
    
    func createTeamData(teams: [TeamResponse]) {
        guard let realm = try? Realm() else { return }
        for team in teams {
            do {
                try realm.write {
                    realm.add(team.managedObject(), update: .modified)
                }
            } catch let error {
                print(error)
            }
        }
    }
    
    func getTeamList(teamHandler: @escaping ([String]) -> Void) {
        guard let realm = try? Realm() else { return }
        var teamNames: [String] = []
        let teamsObj = realm.objects(RealmTeam.self)
        
        for teamObj in teamsObj {
            if let name = teamObj.strTeam {
                teamNames.append(name)
            }
        }
        teamHandler(teamNames)
    }
    
    func getTeamIds(teamHandler: @escaping ([String]) -> Void) {
        guard let realm = try? Realm() else { return }
        var teamIds: [String] = []
        let teamsObj = realm.objects(RealmTeam.self)
        
        for teamObj in teamsObj {
            if let teamId = teamObj.idTeam {
                teamIds.append(teamId)
            }
        }
        teamHandler(teamIds)
        
    }
    
    func createEventData(events: [EventResponse], teamId: String) {
        guard let realm = try? Realm() else { return }
        for event in events {
            do {
                try realm.write {
                    realm.add(event.managedObject(teamId: teamId), update: .modified)
                }
            } catch let error {
                print(error)
            }
        }
    }
    
    func fetchEventsById(teamId: String, completionHandler: @escaping ([EventResponse]) -> Void) {
        guard let realm = try? Realm() else { return }
        var events: [EventResponse] = []
        let eventsObj = realm.objects(RealmEvent.self).filter("teamId = '\(teamId)'")
        for eventObj in eventsObj {
            events.append(EventResponse(managedObject: eventObj))
        }
        
        completionHandler(events)
    }
    
    func createPlayerData(players: [PlayerResponse]) {
        guard let realm = try? Realm() else { return }
        for player in players {
            do {
                try realm.write {
                    realm.add(player.managedObject(), update: .modified)
                }
            } catch let error {
                print(error)
            }
        }
    }
    
    func fetchPlayers(teamId: String, completionHandler: @escaping ([PlayerResponse]) -> Void) {
        guard let realm = try? Realm() else { return }
        var players: [PlayerResponse] = []
        let playersObj = realm.objects(RealmPlayer.self).filter("idTeam = '\(teamId)'")
        for playerObj in playersObj {
            players.append(PlayerResponse(managedObject: playerObj))
        }
        
        completionHandler(players)
    }
    
    func deleteData(entityName: String) {
        let realm = try! Realm()
        switch entityName {
        case TeamEntity.entityName.rawValue:
            let allitems = realm.objects(RealmTeam.self)
            try! realm.write {
                realm.delete(allitems)
            }
        case EventEntity.entityName.rawValue:
            let allitems = realm.objects(RealmEvent.self)
            try! realm.write {
                realm.delete(allitems)
            }
        case PlayerEntity.entityName.rawValue:
            let allitems = realm.objects(RealmPlayer.self)
            try! realm.write {
                realm.delete(allitems)
            }
        default:
            print("Delete item not found")
        }
    }
    
    func isEmpty(entityName: String) -> Bool {
        do {
            let realm = try Realm()
            switch entityName {
            case TeamEntity.entityName.rawValue:
                let allitems = realm.objects(RealmTeam.self)
                return allitems.isEmpty
            case EventEntity.entityName.rawValue:
                let allitems = realm.objects(RealmEvent.self)
                return allitems.isEmpty
            case PlayerEntity.entityName.rawValue:
                let allitems = realm.objects(RealmPlayer.self)
                return allitems.isEmpty
            default:
                return true
            }
        } catch let error {
            print(error)
        }
        return true
    }
}
