//import UIKit
//import CoreData
//
//// MARK: - Unused - Currently using Realm for storing data
//
//
//
//class CoreDataHandler {
//    
//    lazy var persistentContainer: NSPersistentContainer = {
//        
//        let container = NSPersistentContainer(name: "NBA")
//        container.loadPersistentStores(completionHandler: { (_, error) in
//            if let error = error as NSError? {
//                fatalError("Unresolved error \(error), \(error.userInfo)")
//            }
//        })
//        return container
//    }()
//    init() {}
//    
//    private func TeamResponseFromNSObject(data: NSManagedObject) -> TeamResponse {
//        let idTeam = data.value(forKeyPath: TeamEntity.teamId.rawValue)
//        let strTeam = data.value(forKeyPath: TeamEntity.name.rawValue)
//        let strDescriptionEN = data.value(forKeyPath: TeamEntity.description.rawValue)
//        let strTeamBadge = data.value(forKeyPath: TeamEntity.imageUrl.rawValue)
//        let strStadiumThumb = data.value(forKeyPath: TeamEntity.stadiumImageUrl.rawValue)
//        
//        return TeamResponse(idTeam: idTeam as? String,
//                            strTeam: strTeam as? String,
//                            strDescriptionEN: strDescriptionEN as? String,
//                            strTeamBadge: strTeamBadge as? String,
//                            strStadiumThumb: strStadiumThumb as? String)
//    }
//    
//    private func populateTeamData(a: TeamResponse, team: NSManagedObject) {
//        
//        team.setValue(a.idTeam, forKey: TeamEntity.teamId.rawValue)
//        team.setValue(a.strTeam, forKeyPath: TeamEntity.name.rawValue)
//        team.setValue(a.strTeamBadge, forKeyPath: TeamEntity.imageUrl.rawValue)
//        team.setValue(a.strDescriptionEN, forKeyPath: TeamEntity.description.rawValue)
//        team.setValue(a.strStadiumThumb, forKeyPath: TeamEntity.stadiumImageUrl.rawValue)
//    }
//    
//    private func populatePlayerWithData(playerResponse: PlayerResponse, player: NSManagedObject) {
//        
//        player.setValue(playerResponse.idTeam, forKey: PlayerEntity.teamId.rawValue)
//        player.setValue(playerResponse.idPlayer, forKeyPath: PlayerEntity.playerId.rawValue)
//        player.setValue(playerResponse.strPlayer, forKeyPath: PlayerEntity.name.rawValue)
//        player.setValue(playerResponse.strDescriptionEN, forKeyPath: PlayerEntity.description.rawValue)
//        player.setValue(playerResponse.dateBorn, forKeyPath: PlayerEntity.born.rawValue)
//        player.setValue(playerResponse.strHeight, forKeyPath: PlayerEntity.height.rawValue)
//        player.setValue(playerResponse.strWeight, forKeyPath: PlayerEntity.weight.rawValue)
//        player.setValue(playerResponse.strThumb, forKeyPath: PlayerEntity.imageUrl.rawValue)
//        player.setValue(playerResponse.strTeam, forKey: PlayerEntity.teamName.rawValue)
//    }
//    
//    private func PlayerResponseFromNSObject(data: NSManagedObject) -> PlayerResponse {
//        
//        let idTeam = data.value(forKeyPath: PlayerEntity.teamId.rawValue)
//        let idPlayer = data.value(forKeyPath: PlayerEntity.playerId.rawValue)
//        let strTeam = data.value(forKeyPath: PlayerEntity.teamName.rawValue)
//        let strDescriptionEN = data.value(forKeyPath: PlayerEntity.description.rawValue)
//        let strPlayer = data.value(forKeyPath: PlayerEntity.name.rawValue)
//        let dateBorn = data.value(forKeyPath: PlayerEntity.born.rawValue)
//        let strHeight = data.value(forKeyPath: PlayerEntity.height.rawValue)
//        let strWeight = data.value(forKeyPath: PlayerEntity.weight.rawValue)
//        let strThumb = data.value(forKeyPath: PlayerEntity.imageUrl.rawValue)
//        
//        return PlayerResponse(idPlayer: idPlayer as? String,
//                              idTeam: idTeam as? String,
//                              strTeam: strTeam as? String,
//                              strPlayer: strPlayer as? String,
//                              dateBorn: dateBorn as? String,
//                              strHeight: strHeight as? String,
//                              strWeight: strWeight as? String,
//                              strThumb: strThumb as? String,
//                              strDescriptionEN: strDescriptionEN as? String)
//    }
//}
//
//extension CoreDataHandler: DataHandling {
//    
//    // MARK: - Team Operations
//    
//    func fetchTeams(completionHandler: @escaping ([TeamResponse], Error?) -> Void) {
//        
//        var teams: [TeamResponse] = []
//        let managedContext = persistentContainer.viewContext
//        
//        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: TeamEntity.entityName.rawValue)
//        do {
//            let result = try managedContext.fetch(fetchRequest)
//            for data in result as! [NSManagedObject]{
//                
//                teams.append(TeamResponseFromNSObject(data: data))
//            }
//        } catch let err {
//            print(err)
//            completionHandler([], err)
//        }
//        completionHandler(teams, nil)
//    }
//    func createTeamData(teams: [TeamResponse]) {
//        
//        let managedContext = persistentContainer.viewContext
//        
//        let teamEntity = NSEntityDescription.entity(forEntityName: TeamEntity.entityName.rawValue, in: managedContext)!
//        
//        for a in teams {
//            let team = NSManagedObject(entity: teamEntity, insertInto: managedContext)
//            populateTeamData(a: a, team: team)
//        }
//        do {
//            try managedContext.save()
//        } catch let error as NSError {
//            print("Could not save. \(error)")
//        }
//    }
//    
//    func getTeamList(teamHandler: @escaping ([String]) -> Void) {
//        
//        var teams: [String] = []
//        let managedContext = persistentContainer.viewContext
//        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: TeamEntity.entityName.rawValue)
//        
//        do {
//            let result = try managedContext.fetch(fetchRequest)
//            for data in result as! [NSManagedObject] {
//                teams.append(data.value(forKeyPath: TeamEntity.name.rawValue) as! String)
//            }
//        } catch let err {
//            print(err)
//        }
//        teamHandler(teams)
//    }
//    
//    func getTeamIds(teamHandler: @escaping ([String]) -> Void) {
//        
//        var teams: [String] = []
//        let managedContext = persistentContainer.viewContext
//        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: TeamEntity.entityName.rawValue)
//        
//        do {
//            let result = try managedContext.fetch(fetchRequest)
//            for data in result as! [NSManagedObject] {
//                teams.append(data.value(forKeyPath: TeamEntity.teamId.rawValue) as! String)
//            }
//        } catch let err {
//            print(err)
//        }
//        teamHandler(teams)
//    }
//    
//    // MARK: - Event Operations
//    
//    func createEventData(events: [EventResponse], teamId: String) {
//        
//        let managedContext = persistentContainer.viewContext
//        let eventEntity = NSEntityDescription.entity(forEntityName: EventEntity.entityName.rawValue, in: managedContext)!
//        
//        for event in events {
//            let eventObject = NSManagedObject(entity: eventEntity, insertInto: managedContext)
//            eventObject.setValue(teamId, forKey: TeamEntity.teamId.rawValue)
//            eventObject.setValue(event.strAwayTeam, forKeyPath: EventEntity.awayTeam.rawValue)
//            eventObject.setValue(event.strHomeTeam, forKeyPath: EventEntity.homeTeam.rawValue)
//            eventObject.setValue(event.dateEvent, forKeyPath: EventEntity.date.rawValue)
//        }
//        do {
//            try managedContext.save()
//        } catch let error as NSError {
//            print("Could not save. \(error)")
//        }
//    }
//    
//    func fetchEventsById(teamId: String, completionHandler: @escaping ([EventResponse]) -> Void) {
//        var events: [EventResponse] = []
//        
//        let managedContext = persistentContainer.viewContext
//        
//        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: EventEntity.entityName.rawValue)
//        do {
//            let result = try managedContext.fetch(fetchRequest)
//            for data in result as! [NSManagedObject] {
//                if data.value(forKeyPath: TeamEntity.teamId.rawValue) as! String == teamId {
//                    let awayTeam = data.value(forKeyPath: EventEntity.awayTeam.rawValue)
//                    let homeTeam = data.value(forKeyPath: EventEntity.homeTeam.rawValue)
//                    let date = data.value(forKeyPath: EventEntity.date.rawValue)
//                    
//                    events.append(EventResponse(strHomeTeam: homeTeam as? String,
//                                                strAwayTeam: awayTeam as? String,
//                                                dateEvent: date as? String))
//                }
//            }
//        } catch let err {
//            print(err)
//        }
//        completionHandler(events)
//    }
//    
//    // MARK: - Player Operations
//    
//    func createPlayerData(players: [PlayerResponse]) {
//        
//        let managedContext = persistentContainer.viewContext
//        
//        guard let playerEntity = NSEntityDescription.entity(forEntityName: PlayerEntity.entityName.rawValue,
//                                                            in: managedContext) else { return }
//        
//        for a in players {
//            let player = NSManagedObject(entity: playerEntity, insertInto: persistentContainer.viewContext)
//            populatePlayerWithData(playerResponse: a, player: player)
//            
//        }
//        do {
//            try managedContext.save()
//        } catch let error as NSError {
//            print("Could not save. \(error)")
//        }
//    }
//    
//    func fetchPlayers(teamId: String, completionHandler: @escaping ([PlayerResponse]) -> Void) {
//        var players: [PlayerResponse] = []
//        
//        let managedContext = persistentContainer.viewContext
//        
//        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: PlayerEntity.entityName.rawValue)
//        do {
//            let result = try managedContext.fetch(fetchRequest)
//            for data in result as! [NSManagedObject] {
//                if data.value(forKeyPath: PlayerEntity.teamId.rawValue) as? String == teamId {
//                    players.append(PlayerResponseFromNSObject(data: data))
//                }
//            }
//        } catch let error {
//            print(error)
//        }
//        completionHandler(players)
//    }
//    
//    // MARK: - Other Operations
//    
//    func deleteData(entityName: String) {
//        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
//        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
//        
//        do {
//            try persistentContainer.viewContext.execute(deleteRequest)
//        } catch let error as NSError {
//            print(error)
//        }
//    }
//    
//    func isEmpty(entityName: String) -> Bool {
//        
//        let managedContext = persistentContainer.viewContext
//        
//        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
//        
//        do {
//            let result = try managedContext.fetch(fetchRequest)
//            return result.isEmpty
//        } catch let err {
//            print(err)
//        }
//        return true
//    }
//}
