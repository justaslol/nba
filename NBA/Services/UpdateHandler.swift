import Foundation

protocol UpdateHandling {
    func shouldLoadFromApi(id: UpdateID) -> Bool
    func resetDefaults()
    func updateTime(id: String)
    func getTeamData(completionHandler: @escaping ([TeamResponse], Error?) -> Void)
    func loadTeamsFromApiToCD(teamHandler: @escaping (Error?) -> Void)
    func getEventData(teamId: String, completionHandler: @escaping ([EventResponse]) -> Void)
    func loadEventsFromApiToCD(eventHandler: @escaping (Error?) -> Void)
    func getPlayerData(teamId: String, completionHandler: @escaping ([PlayerResponse]) -> Void)
    func loadPlayersFromApiToCD(playerHandler: @escaping (Error?) -> Void)
}

class UpdateHandler {
    
    let defaults: UserDefaults
    let data: DataHandling
    let apiHandler: APIHandling
    
    convenience init(data: DataHandling, apiHandler: APIHandling) {
        self.init(data: data, apiHandler: apiHandler, defaults: UserDefaults.standard)
    }
    
    init(data: DataHandling, apiHandler: APIHandling, defaults: UserDefaults) {
        self.data = data
        self.apiHandler = apiHandler
        self.defaults = defaults
    }
}

extension UpdateHandler: UpdateHandling {
        
    func shouldLoadFromApi(id: UpdateID) -> Bool {
        
        if var lastUpdate = defaults.object(forKey: id.rawValue) as? Date {
            switch id {
            case .team:
                lastUpdate = lastUpdate.addingTimeInterval(Double(UpdateInterval.team.rawValue))
            case .event:
                lastUpdate = lastUpdate.addingTimeInterval(Double(UpdateInterval.event.rawValue))
            case .player:
                lastUpdate = lastUpdate.addingTimeInterval(Double(UpdateInterval.player.rawValue))
            }
            if lastUpdate < Date() {
                return true
            } else {
                return false
            }
        } else {
            return true
        }
    }
    
    func resetDefaults() {
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            defaults.removeObject(forKey: key)
        }
    }
    
    func updateTime(id: String) {
        defaults.set(Date(), forKey: id)
    }
    
    func getTeamData(completionHandler: @escaping ([TeamResponse], Error?) -> Void) {
        if try shouldLoadFromApi(id: UpdateID.team)
            || data.isEmpty(entityName: TeamEntity.entityName.rawValue) {
            print("fetching Teams from API...")
            loadTeamsFromApiToCD { (error) in
                if let err = error {
                    completionHandler([], err)
                }
                self.data.fetchTeams { (teams, error)  in
                    if let err = error {
                        completionHandler(teams, err)
                    }
                    completionHandler(teams, nil)
                }
            }
            updateTime(id: UpdateID.team.rawValue)
        } else {
            data.fetchTeams { (teams, error) in
                if let err = error {
                    completionHandler(teams, err)
                }
                completionHandler(teams, nil)
            }
        }
    }
    
    func loadTeamsFromApiToCD(teamHandler: @escaping (Error?) -> Void) {
        apiHandler.getTeamsFromAPI { (teams, error) in
            if let err = error {
                teamHandler(err)
            }
            self.data.createTeamData(teams: teams)
            teamHandler(nil)
        }
    }
    
    func getEventData(teamId: String, completionHandler: @escaping ([EventResponse]) -> Void) {
        if  shouldLoadFromApi(id: UpdateID.event)
            || data.isEmpty(entityName: EventEntity.entityName.rawValue) {
            data.deleteData(entityName: EventEntity.entityName.rawValue)
            var eventResponse: [EventResponse] = []
            
            loadEventsFromApiToCD { (_) in
                self.data.fetchEventsById(teamId: teamId) { (events) in
                    eventResponse = events
                }
                completionHandler(eventResponse)
            }
            updateTime(id: UpdateID.event.rawValue)
        } else {
            data.fetchEventsById(teamId: teamId) { (events) in
                completionHandler(events)
            }
        }
    }
    
    func loadEventsFromApiToCD(eventHandler: @escaping (Error?) -> Void) {
        data.getTeamIds {(teams) in
            for id in teams {
                self.apiHandler.getEventsFromAPI(teamId: id) { (events, error) in
                    if let error = error {
                        print(error)
                    }
                    self.data.createEventData(events: events, teamId: id)
                    eventHandler(error)
                }
            }
        }
    }
    
    func getPlayerData(teamId: String, completionHandler: @escaping ([PlayerResponse]) -> Void) {
        if  shouldLoadFromApi(id: UpdateID.player)
            || data.isEmpty(entityName: PlayerEntity.entityName.rawValue) {
            var playerResponse: [PlayerResponse] = []
            loadPlayersFromApiToCD { (_) in
                self.data.fetchPlayers(teamId: teamId) { (players) in
                    playerResponse = players
                }
                completionHandler(playerResponse)
            }
            updateTime(id: UpdateID.player.rawValue)
        } else {
            data.fetchPlayers(teamId: teamId) { (players) in
                completionHandler(players)
            }
        }
    }
    
    func loadPlayersFromApiToCD(playerHandler: @escaping (Error?) -> Void) {
        data.getTeamList {(teams) in
            for name in teams {
                self.apiHandler.getPlayersFromAPI(teamName: name) { (players, error) in
                    if let error = error {
                        print("Error getting players from API: \(error)")
                    }
                    self.data.createPlayerData(players: players)
                    playerHandler(error)
                }
            }
        }
    }
}
