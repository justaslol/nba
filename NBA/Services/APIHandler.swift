import Foundation
import Alamofire

enum Urls: String {
    case base = "https://www.thesportsdb.com/api/v1/json/1"
    case teams = "https://www.thesportsdb.com/api/v1/json/1/lookup_all_teams.php/?id=4387"
    case events = "https://www.thesportsdb.com/api/v1/json/1/eventslast.php?id="
    case players = "https://www.thesportsdb.com/api/v1/json/1/searchplayers.php?t="
}

protocol APIHandling {
    func getTeamsFromAPI( completionHandler: @escaping ([TeamResponse], Error?) -> Void)
    func getEventsFromAPI(teamId: String, completionHandler: @escaping ([EventResponse], Error?) -> Void)
    func getPlayersFromAPI(teamName: String, completionHandler: @escaping ([PlayerResponse], Error?) -> Void)
}

class APIHandler {
}

extension APIHandler: APIHandling {
    func getTeamsFromAPI( completionHandler: @escaping ([TeamResponse], Error?) -> Void) {
        let url = URL(string: Urls.teams.rawValue)!

        AF.request(url)
            .validate()
            .responseData { response in
                switch response.result {
                case .success:
                    if let data = response.data,
                    let teamsArray = try? JSONDecoder().decode(TeamsResponse.self, from: data) {
                      completionHandler(teamsArray.teams ?? [], nil)
                    }
                case let .failure(error):
                    print(error)
                    completionHandler([], error)
                }
        }.resume()
      }

    func getEventsFromAPI(teamId: String, completionHandler: @escaping ([EventResponse], Error?) -> Void) {

        if let url = URL(string: "\(Urls.events.rawValue)\(teamId)") {
            AF.request(url)
                .validate()
                .responseData { response in
                    switch response.result {
                    case .success:
                        if let data = response.data,
                        let eventsArray = try? JSONDecoder().decode(EventsResponse.self, from: data) {
                          completionHandler(eventsArray.events ?? [], nil)
                        }
                    case let .failure(error):
                        print(error)
                        completionHandler([], error)
                    }
            }.resume()
        }
    }

    func getPlayersFromAPI(teamName: String, completionHandler: @escaping ([PlayerResponse], Error?) -> Void) {

        let fullUrl = "\(Urls.players.rawValue)\(teamName.replacingOccurrences(of: " ", with: "%20"))"
      let url = URL(string: fullUrl)!
      AF.request(url)
          .validate()
          .responseData { response in
            switch response.result {
            case .success:
                if let data = response.data,
                let playersArray = try? JSONDecoder().decode(PlayersResponse.self, from: data) {
                  completionHandler(playersArray.players ?? [], nil)
                }
            case let .failure(error):
                print(error)
                completionHandler([], error)
            }
      }.resume()
    }
}
