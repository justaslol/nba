@testable import NBA

class MockUpdateHandler {
    var shouldReturnError = false
    var shouldLoadFromApi = false
    
    enum MockUpdateHandlerError: Error {
        case getTeams
        case getEvents
        case getPlayers
    }
    
    convenience init() {
        self.init(false)
    }
    init(_ shouldReturnError: Bool) {
        self.shouldReturnError = shouldReturnError
    }
    
    let mockTeamResponse: TeamsResponse = TeamsResponse(teams: [
        TeamResponse(idTeam: "134880"
            , strTeam: "Atlanta Hawks"
            , strDescriptionEN:  "test"
            , strTeamBadge: "https://www.thesportsdb.com/images/media/team/badge/cfcn1w1503741986.png"
            , strStadiumThumb: "https://www.thesportsdb.com/images/media/team/stadium/qxstqs1420567922.jpg")
        ,
        TeamResponse(idTeam: "134860"
            , strTeam: "Boston Celtics"
            , strDescriptionEN: "test"
            , strTeamBadge: "https://www.thesportsdb.com/images/media/team/badge/051sjd1537102179.png"
            , strStadiumThumb: "https://www.thesportsdb.com/images/media/team/stadium/yuyrvx1420576364.jpg")
    ])
    let mockEventResponse: EventsResponse = EventsResponse(events:
        [
            EventResponse(strHomeTeam: "team1",
                          strAwayTeam: "team2",
                          dateEvent: "date"),
            EventResponse(strHomeTeam: "team1",
                          strAwayTeam: "team2",
                          dateEvent: "date")])
    let mockPlayerResponse: PlayersResponse = PlayersResponse(
        players:
        [
            PlayerResponse(idPlayer: "test",
                           idTeam: "test",
                           strTeam: "Test",
                           strPlayer: "test",
                           dateBorn: "test",
                           strHeight: "Test",
                           strWeight: "tetst",
                           strThumb: "test",
                           strDescriptionEN: "test"),
            PlayerResponse(idPlayer: "test",
                           idTeam: "test",
                           strTeam: "test",
                           strPlayer: "test",
                           dateBorn: "test",
                           strHeight: "test",
                           strWeight: "test",
                           strThumb: "test",
                           strDescriptionEN: "test")])
}
extension MockUpdateHandler: UpdateHandling {
    func shouldLoadFromApi(id: UpdateID) -> Bool {
        return shouldLoadFromApi
    }
    func resetDefaults() {
        return
    }
    func updateTime(id: String) {
        return
    }
    func getTeamData(completionHandler: @escaping ([TeamResponse], Error?) -> Void) {
        if shouldReturnError {
            completionHandler([], MockUpdateHandlerError.getTeams)
        } else {
            completionHandler(mockTeamResponse.teams!, nil)
        }
    }
    func loadTeamsFromApiToCD(teamHandler: @escaping (Error?) -> Void) {
        if shouldReturnError {
            teamHandler(MockUpdateHandlerError.getTeams)
        } else {
            teamHandler(nil)
        }
    }
    func getEventData(teamId: String, completionHandler: @escaping ([EventResponse]) -> Void) {
        if shouldReturnError {
            completionHandler([])
        } else {
            completionHandler(mockEventResponse.events!)
        }
    }
    func loadEventsFromApiToCD(eventHandler: @escaping (Error?) -> Void) {
        if shouldReturnError {
            eventHandler(MockUpdateHandlerError.getEvents)
        } else {
            eventHandler(nil)
        }
    }
    func getPlayerData(teamId: String, completionHandler: @escaping ([PlayerResponse]) -> Void) {
        if shouldReturnError {
            completionHandler([])
        } else {
            completionHandler(mockPlayerResponse.players!)
        }
    }
    func loadPlayersFromApiToCD(playerHandler: @escaping (Error?) -> Void) {
        if shouldReturnError {
            playerHandler(MockUpdateHandlerError.getPlayers)
        } else {
            playerHandler(nil)
        }
    }
}
