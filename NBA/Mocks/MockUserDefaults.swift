import XCTest
@testable import NBA

class MockUserDefaults : UserDefaults {
    
    var setDefaultName = ""
    var removedObjectName = ""
    var objectNameFromId = ""
    
    convenience init() {
        self.init(suiteName: "Mock User Defaults")!
    }
    
    override init?(suiteName suitename: String?) {
        UserDefaults().removePersistentDomain(forName: suitename!)
        super.init(suiteName: suitename)
    }
    
    override func set(_ value: Any?, forKey defaultName: String) {
        setDefaultName = defaultName
    }
    
    override func removeObject(forKey defaultName: String) {
        removedObjectName = defaultName
    }
    
    override func object(forKey defaultName: String) -> Any? {
        switch defaultName {
        case "1":
            objectNameFromId = "team"
            return Date()
        case "2":
            objectNameFromId = "event"
            return Date()
        case "3":
            objectNameFromId = "player"
            return Date()
        default:
            objectNameFromId = "error"
        }
        return Date()
    }
    
}
