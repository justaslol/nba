import XCTest
@testable import NBA

class MockAPIHandler {

    var shouldReturnError = false
    var getTeamsWasCalled = false
    var getEventsWasCalled = false
    var getPlayersWasCalled = false
    
    enum MockAPIHandlerError: Error {
        case getTeams
        case getEvents
        case getPlayers
    }

    convenience init() {
        self.init(false)
    }
    init(_ shouldReturnError: Bool) {
        self.shouldReturnError = shouldReturnError
    }

    let mockTeamResponse: TeamsResponse = TeamsResponse(teams: [
        TeamResponse(idTeam: "134880"
            , strTeam: "Atlanta Hawks"
            , strDescriptionEN:  "test"
            , strTeamBadge: "https://www.thesportsdb.com/images/media/team/badge/cfcn1w1503741986.png"
            , strStadiumThumb: "https://www.thesportsdb.com/images/media/team/stadium/qxstqs1420567922.jpg")
        ,
        TeamResponse(idTeam: "134860"
            , strTeam: "Boston Celtics"
            , strDescriptionEN: "test"
            , strTeamBadge: "https://www.thesportsdb.com/images/media/team/badge/051sjd1537102179.png"
            , strStadiumThumb: "https://www.thesportsdb.com/images/media/team/stadium/yuyrvx1420576364.jpg")
        ])
    let mockEventResponse: EventsResponse = EventsResponse(events:
        [
            EventResponse(strHomeTeam: "team1",
                          strAwayTeam: "team2",
                          dateEvent: "date"),
            EventResponse(strHomeTeam: "team1",
                  strAwayTeam: "team2",
                  dateEvent: "date")])
    let mockPlayerResponse: PlayersResponse = PlayersResponse(
        players:
        [
            PlayerResponse(idPlayer: "test",
                           idTeam: "test",
                           strTeam: "Test",
                           strPlayer: "test",
                           dateBorn: "test",
                           strHeight: "Test",
                           strWeight: "tetst",
                           strThumb: "test",
                           strDescriptionEN: "test"),
            PlayerResponse(idPlayer: "test",
                           idTeam: "test",
                           strTeam: "test",
                           strPlayer: "test",
                           dateBorn: "test",
                           strHeight: "test",
                           strWeight: "test",
                           strThumb: "test",
                           strDescriptionEN: "test")])

}

extension MockAPIHandler: APIHandling {
    func getTeamsFromAPI( completionHandler: @escaping ([TeamResponse], Error?) -> Void) {
        getTeamsWasCalled = true

        if shouldReturnError {
            completionHandler([], MockAPIHandlerError.getTeams)
        } else {
            completionHandler(mockTeamResponse.teams!, nil)
        }
    }
    func getEventsFromAPI(teamId: String, completionHandler: @escaping ([EventResponse], Error?) -> Void) {
        getEventsWasCalled = true
        if shouldReturnError {
            completionHandler([], MockAPIHandlerError.getEvents)
        } else {
            completionHandler(mockEventResponse.events!, nil)
        }
    }
    func getPlayersFromAPI(teamName: String, completionHandler: @escaping ([PlayerResponse], Error?) -> Void) {
        getPlayersWasCalled = true
        if shouldReturnError {
            completionHandler([], MockAPIHandlerError.getPlayers)
        } else {
            completionHandler(mockPlayerResponse.players!, nil)

        }
    }
}
