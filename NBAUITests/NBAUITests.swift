
import XCTest

class NBAUITests: XCTestCase {

    var app: XCUIApplication!
    
    override func setUp() {
        app = XCUIApplication()
        app.launch()
    }

    override func tearDown() {
        app = nil
        super.tearDown()
    }

    
    
    func testOpenTeam() {
        
        
        let app = XCUIApplication()
        let tablesQuery = app.tables
        let label = app.staticTexts["Boston Celtics"]
        let exists = NSPredicate(format: "exists == 1")

        expectation(for: exists, evaluatedWith: label, handler: nil)
        waitForExpectations(timeout: 5, handler: nil)
        tablesQuery/*@START_MENU_TOKEN@*/.cells.containing(.staticText, identifier:"The Atlanta Hawks are an American professional basketball team based in Atlanta, Georgia. They are part of the Southeast Division of the Eastern Conference in the National Basketball Association (NBA). They play their home games at Philips Arena in Downtown Atlanta.\r\n\r\nTheir origins can be traced to the establishment of the Buffalo Bisons in 1946, a member of the National Basketball League. After 13 games of their inaugural season, the team moved to Moline, Illinois and became the Tri-Cities Blackhawks. In 1949, they joined the National Basketball Association (NBA) as part of the National Basketball League and the Basketball Association of America merger. In 1951, the team moved to Milwaukee, where they changed their name to the Hawks. The team moved again in 1955 to St. Louis, where they won their only NBA Championship in 1958. The Hawks moved to Atlanta in 1968, where they have been ever since.\r\n\r\nThe Hawks currently own the second-longest run (behind the Sacramento Kings) of not winning an NBA title at 56 years. All the franchise's NBA Finals appearances and lone NBA championship took place when the team resided in St. Louis. Meanwhile, since the elimination of first-round byes in 1967, they have not advanced beyond the second round in any playoff format. Much of the failure they've experienced in the postseason can be traced back to their poor history in the NBA draft. Since 1980, the Hawks have drafted only three players who have been chosen to play in an All-Star game (Doc Rivers, Kevin Willis, and Al Horford; Dominique Wilkins was actually selected by the Utah Jazz and traded to the Hawks a few months after the draft). Horford is the only All-Star Hawk to have been drafted since Willis was selected in 1984, and is also the only first-rounder the Hawks selected in their nine-year playoff drought to play in an NBA All-Star Game.")/*[[".cells.containing(.staticText, identifier:\"Atlanta Hawks\")",".cells.containing(.staticText, identifier:\"The Atlanta Hawks are an American professional basketball team based in Atlanta, Georgia. They are part of the Southeast Division of the Eastern Conference in the National Basketball Association (NBA). They play their home games at Philips Arena in Downtown Atlanta.\\r\\n\\r\\nTheir origins can be traced to the establishment of the Buffalo Bisons in 1946, a member of the National Basketball League. After 13 games of their inaugural season, the team moved to Moline, Illinois and became the Tri-Cities Blackhawks. In 1949, they joined the National Basketball Association (NBA) as part of the National Basketball League and the Basketball Association of America merger. In 1951, the team moved to Milwaukee, where they changed their name to the Hawks. The team moved again in 1955 to St. Louis, where they won their only NBA Championship in 1958. The Hawks moved to Atlanta in 1968, where they have been ever since.\\r\\n\\r\\nThe Hawks currently own the second-longest run (behind the Sacramento Kings) of not winning an NBA title at 56 years. All the franchise's NBA Finals appearances and lone NBA championship took place when the team resided in St. Louis. Meanwhile, since the elimination of first-round byes in 1967, they have not advanced beyond the second round in any playoff format. Much of the failure they've experienced in the postseason can be traced back to their poor history in the NBA draft. Since 1980, the Hawks have drafted only three players who have been chosen to play in an All-Star game (Doc Rivers, Kevin Willis, and Al Horford; Dominique Wilkins was actually selected by the Utah Jazz and traded to the Hawks a few months after the draft). Horford is the only All-Star Hawk to have been drafted since Willis was selected in 1984, and is also the only first-rounder the Hawks selected in their nine-year playoff drought to play in an NBA All-Star Game.\")"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.children(matching: .other).element(boundBy: 0).tap()
        app/*@START_MENU_TOKEN@*/.buttons["Players"]/*[[".segmentedControls.buttons[\"Players\"]",".buttons[\"Players\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["Alex Poythress "]/*[[".cells.staticTexts[\"Alex Poythress \"]",".staticTexts[\"Alex Poythress \"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
     
    }

}
