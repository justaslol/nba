@testable import NBA

import XCTest

class NBAViewControllerTests: XCTestCase {
    
    var sut: UIViewController!

    override func setUp() {
        super.setUp()
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TeamsViewController") as! TeamsViewController
        sut = vc
        sut.loadViewIfNeeded()
    }

    override func tearDown() {

        sut = nil
        super.tearDown()
        
    }

}
