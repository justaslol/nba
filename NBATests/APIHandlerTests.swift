import XCTest

@testable import NBA

class APIHandlerTests: XCTestCase {
    
    var apiHandler: APIHandling = APIHandler()

    func testGetTeamsFromApiSuccess() {
        
        apiHandler.getTeamsFromAPI { (teams, error) in
            XCTAssertNil(error)
            XCTAssertFalse(teams.isEmpty)
        }
    }
    
    func testGetTeamsFromApiFail() {
        apiHandler.getTeamsFromAPI { (teams, error) in
            XCTAssertNotNil(error)
            XCTAssertTrue(teams.isEmpty, "Team array is not empty")
        }
    }
    
    func testGetEventsFromApiSuccess() {
        apiHandler.getEventsFromAPI(teamId: "test"){ (events, error) in
            XCTAssertNil(error)
            XCTAssertFalse(events.isEmpty)
        }
    }
    
    func testGetEventsFromApiFail() {
        apiHandler.getEventsFromAPI(teamId: "test"){ (events, error) in
            XCTAssertNotNil(error)
            XCTAssertTrue(events.isEmpty)
        }
    }
    
    func testGetPlayersFromApiSuccess() {
        apiHandler.getPlayersFromAPI(teamName: "test"){ (players, error) in
            XCTAssertNil(error)
            XCTAssertFalse(players.isEmpty)
        }
    }
    
    func testGetPlayersFromApiFail() {
        apiHandler.getPlayersFromAPI(teamName: "test"){ (players, error) in
            XCTAssertNotNil(error)
            XCTAssertTrue(players.isEmpty)
        }
    }
}
