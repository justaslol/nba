

import XCTest
@testable import NBA

class MockAPIHandler: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        measure {
            // Put the code you want to measure the time of here.
        }
    }

}

extension MockAPIHandler: APIHandlerProtocol {
    func getTeamsFromAPI( completionHandler: @escaping ([TeamResponse], Error?) -> Void){}
    func getEventsFromAPI(teamId: String, completionHandler: @escaping ([EventResponse]) -> Void){}
    func getPlayersFromAPI(teamName: String, completionHandler: @escaping ([PlayerResponse]) -> Void){}
}
