import XCTest
@testable import NBA

class UpdateHandlerTests: XCTestCase {
    
    var updateHandler: UpdateHandling = UpdateHandler(data: MockCoreDataHandler(), apiHandler: MockAPIHandler(), defaults: MockUserDefaults())
    
    func testShouldLoadFromApi() {
        XCTAssertFalse(updateHandler.shouldLoadFromApi(id: UpdateID.team))
    }
    
    func testResetDefaults() {
        
    }
    
//    func testupdateTime() {
//        updateHandler.updateTime(id: "test")
//      XCTAssertEqual("test", updateHandler.defaults.setDefaultName)
//    }

    func testGetTeamData() {
        updateHandler.getTeamData{ (teams, error) in
            XCTAssertNil(error)
            XCTAssertTrue(teams[0].idTeam! == "134880")
        }
    }
    
    func testLoadTeamsFromApiToCD() {
        updateHandler.loadTeamsFromApiToCD{(error) in
            XCTAssertNil(error)
        }
    }
    
    func testGetEventData() {
        updateHandler.getEventData(teamId: "testid"){ (events) in
            XCTAssertTrue(events[0].strHomeTeam! == "team1")
        }
    }
    
    func testLoadEventsFromApiToCD() {
        updateHandler.loadEventsFromApiToCD{ (error) in
            XCTAssertNil(error)
        }
    }
    
    func testGetPlayerData() {
        updateHandler.getPlayerData (teamId: "testid"){ (players) in
            XCTAssertTrue(players[0].idPlayer! == "test")
        }
    }
    
    func testLoadPlayersFromApiToCD() {
        updateHandler.loadPlayersFromApiToCD{ (error) in
            XCTAssertNil(error)
        }
    }
    
}
